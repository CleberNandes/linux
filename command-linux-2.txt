------------------------LINUX 2  
Processos - Processes 
ps     | show cmd process 
ps -e  | show all processes 
ps -ef | show all processes with all info  search a specific process 
ps -ef | grep mysql  

kill a process avisan o processo para ele parar 
kill process_id -9 

mata direto sem pedir ao processo parar 
kill -9 process_id   

Windows  
copy nul file-name.txt  //cria um arquivo de txt no windows 
type nul > file-name.txt     //cria um arquivo de txt no windows 
echo. > file-name.txt        //cria um arquivo de txt no windows

AULA 02

top			=> show all situations from process, memory, processer
ps -ef | grep top	=> show all tops are running
kill -9 number_process  => kill top process
killall -9 name		=> kill all that has a specific name
top -u user_name	=> show just the processes being used for the specific user passed
top -p process_number	=> show just the one process and its info
while top is running press keyboard to change the time to update processes, after you insert the new value (in seconds)

the difference between top and ps is that top update the info when it change the consume
-9 make sure that kill a process

AULA 03

running a program by terminal
ex firefox
pstree			=> show tree for all process
cmd + z 		=> stopped a bash when running firefox
if we want to keep bash free 
jobs			=> see all jobs sttoped or running
bg id_job		=> put job in background and keep bash free
fg id_job		=> put job in forground and keep bash busy
bg 				=> put last job in background
fg 				=> put last job in forground

gedit 			=> text editor

program &		=> means that the program are running background
program &		=> lanch the program straight in background
cmd + c 		=> kill the program in forground

AULA 04

creating scripts bash
gedit dorme &	=> creating a script with gedit with terminal free

put commands on scripts
	echo "vou dormir"
	sleep 5
	echo "terminei de dormir"
sh 				=> represents shortcut bash

sh dorme		=> to execute the new script
bash dorme		=> to execute the new script

programs permissions
d 				=> directory
r 				=> read
w 				=> write
x				=> execute

permissions mean
-rwxr-xr-x 1 root root 52 Aug 12 14:52 dorme

change permissions
ls -l 			=> to see permissions from a specific directory
chmod +x dorme	=> change dorme permission to be execute to all users
chmod -x dorme  => take off permission to be execute from all users

after change permission the script can be execute without sh/bash
just telling where script is with ./
./dorme			=> execute script dorme without sh

script to backup | zip dir
~/				=> raiz do usuario
