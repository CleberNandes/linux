exec { "apt-update":
	command => "/usr/bin/apt-get update"
}
package { ["openjdk-7-jre", "tomcat7", "unzip", "mysql-server"]:
	ensure  => installed,
	require => Exec["apt-update"]
}
service { "tomcat7":
	ensure     => running,
	enable     => true,
	hasstatus  => true,
	hasrestart => true,
	require    => Package["tomcat7"]
}
file { "/var/lib/tomcat7/webapps/vraptor-musicjungle.war":
	source  => "/vagrant/manifests/vraptor-musicjungle.war",
	owner   => tomcat7,
	group   => tomcat7,
	mode    => 0644,
	require => Package["tomcat7"],
	notify  => Service["tomcat7"]
}
service { "mysql":
	ensure     => running,
	enable     => true,
	hasstatus  => true,
	hasrestart => true,
	require    => Package["mysql-server"]
}
exec { "musicjungle":
	command => "mysqladmin -uroot create musicjungle",
	#só executa o comando se o banco não existir
	unless  => "mysql -uroot musicjungle",
	path    => "/usr/bin",
	require => Service["mysql"]
}
exec { "protect-data":
	command => "mysql -uroot -e \"GRANT ALL PRIVILEGES ON * TO 'cleber'@'%' IDENTIFIED BY '123123';\" musicjungle",
	require => Exec["musicjungle"],
	path    => "/usr/bin",
	unless  => "mysql -u cleber -p 123123 musicjungle"
}
define file_line ($file, $line){
	exec { "/bin/echo '${line}' >> '${file}'":
		unless => "/bin/grep -qFx '${line}' '${file}'"
	}
}

file_line { "production":
	file    => "/etc/default/tomcat7",
	line    => "JAVA_OPTS=\"\$JAVA_OPTS -Dbr.com.caelum.vraptor.environment=production\"",
	require => Package["tomcat7"],
	notify  => Service["tomcat7"]
}